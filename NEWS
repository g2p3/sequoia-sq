                                                              -*- org -*-
#+TITLE: sequoia-sq NEWS – history of user-visible changes
#+STARTUP: content hidestars

* Changes in 0.29
** New functionality
   - `sq` now supports and implicitly uses a certificate store.  By
     default, `sq` uses the standard OpenPGP certificate directory.
     This is located at `$HOME/.local/share/pgp.cert.d` on XDG
     compliant systems.
   - `sq --no-cert-store`: A new switch to disable the use of the
     certificate store.
   - `sq --cert-store`: A new option to use an alternate certificate
     store.  Currently, only OpenPGP certificate directories are
     supported.
   - `sq import`: A new command to import certificates into the
     certificate store.
   - `sq export`: A new command to export certificates from the
     certificate store.
   - `sq encrypt --recipient-cert`: A new option to specify a
     recipient's certificate by fingerprint or key ID, which is then
     looked up in the certificate store.
   - `sq verify --signer-cert`: A new option to specify a signer's
     certificate by fingerprint or key ID, which is then looked up in
     the certificate store.
   - `sq verify` now also implicitly looks for missing certificates in
     the certificate store.  But, unless they are explicitly named
     using `--signer-cert`, they are not considered authenticated and
     the verification will always fail.
   - `sq certify`: If the certificate to certify is a fingerprint or
     Key ID, then the corresponding certificate is looked up in the
     certificate store.
   - Add a global option, `--time`, to set the reference time.  This
     option replaces the various subcommand's `--time` argument as
     well as `sq key generate` and `sq key userid add`'s
     `--creation-time` arguments.
   - Add top-level option, `--trust-root`, to allow the user to
     specify trust roots.
   - Extend `sq encrypt` to allow addressing recipients by User ID
     (`--recipient-userid`) or email address (`--recipient-email`).
     Only User IDs that can be fully authenticated are considered.
   - Extend `sq verify` to verify certificates looked up from the
     certificate store using the web of trust.  If the signature
     includes a Signer's User ID packet, and the binding can be fully
     authenticated, consider the signature to be authenticated.  If
     there is no Signer's User ID packet, consider the signature to be
     authenticated if any binding can fully be authenticated.
   - Add `sq link add`, which uses the local trust root to
     certify the specified bindings.
   - Add `sq link retract`, which retracts certifications made by the
     local trust root on the specified bindings.
   - Add `sq link list`, which lists the links.
   - Add a top-level option, `--keyring`, to allow the user to specify
     additional keyrings to search for certificates.
   - Import web of trust subcommands from sq-wot.  Specifically, add:
     - `sq wot authenticate` to authenticate a binding.
     - `sq wot lookup` to find a certificate with a particular User ID.
     - `sq wot identify` to list authenticated bindings for a
       certificate.
     - `sq wot list` to list authenticated bindings.
     - `sq wot path` to authenticate and lint a path in a web of trust.
   - `sq keyserver get`, `sq wkd get`, and `sq dane get` now import any
     certificates into the certificate store by default instead of
     exporting them on stdout.  It is still possible to export them
     using the `--output` option.
   - When `sq keyserver get` (for verifying key servers), `sq wkd get`,
     or `sq dane get` saves a certificate to the local certificate
     store, `sq` certifies the validated User IDs (all returned User
     IDs in the case of verifying key servers; User IDs that contain
     the looked up email address in the case of WKD and DANE) using a
     local service-specific proxy CA.  If the proxy key doesn't exist,
     it is created, and certified as a minimally trusted CA (trust
     amount 1 of 120) by the local trust root.  The proxy certificates
     can be managed in the usual way using `sq link add` and `sq link
     retract`.
   - Extend `sq inspect` to inspect certificates from the certificate
     store using the `--cert` option.
 * Deprecated functionality
   - `sq key generate --creation-time TIME` is deprecated in favor of
     `sq key generate --time TIME`.
   - `sq key user id --creation-time TIME` is deprecated in favor of
     `sq user id --time TIME`.
 * Started the NEWS file.
